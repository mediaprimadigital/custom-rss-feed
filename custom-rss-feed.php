<?php
/*
 * Plugin Name:   Custom RSS Feed Plugin
 * Description:   A plugin to customize the default RSS Feed
 * Version:       1.0
 * Author:        Ahmad Muhaimi
 */

function flipboard_attached_images() {
    global $post;
    ?>
    <media:thumbnail><?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ) ?></media:thumbnail>
    <?php

}
add_filter( 'rss2_item', 'flipboard_attached_images' );